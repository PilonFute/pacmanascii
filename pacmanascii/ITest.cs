namespace pacmanascii
{
    interface ITest{

        /*virtual protected void write OK
        Cette fonction écrit la chaine "[OK] : "
        dans la console avec la sous chaîne OK en vert.
        */
        virtual protected void writeOK() {}

        /*virtual protected void write KO
        Cette fonction écrit la chaine "[KO] : "
        dans la console avec la sous chaîne KO en rouge.
        */
        virtual protected void writeKO() {}

        /*abstract public bool test
            Cette fonction exécute le test.
        */
        abstract public bool test();

        /* virtual public void Main(string[] args)
            Cette fonction écecute le test et affiche un résultat.
            Elle prends en argument un tableau de chaine de caractères.
        */
        virtual public void Main(string[] args) {}
    }
}