using System;

namespace pacmanascii
{
    class SumNEntiers
    {
        private int n = 42;
        
        public int N {
            get => n;
            set => n = value;
        }

        private int sum(){
            int i = 0;
            int sum = 0;
            while (i < n) {
                sum = sum + i;
                i = i+1;
            }
            return sum;
        }
    }
}




