using System;

namespace pacmanascii
{
    class TestSumNEntiers : AbstractTest
    {
        public TestSumNEntiers(string name) : base(name) {}

        private int oracle(int n){
            int test;
            if (n == 0) {
                test = 0;
            } else {
                int n1 = n-1;
                test = (n1*(n1+1))/2;
            }
            return test;
        }
        
        override public bool test(){
            SumNEntiers sum = new SumNEntiers();
            //sum.N = 42
            if (sum.N == 42){
                return true;
            } else {
                return false;
            }
        }
    }
}


