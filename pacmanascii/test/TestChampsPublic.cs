using System;

namespace pacmanascii
{
    class TestChampsPublic : AbstractTest
    {
        /*Implémentation tu constructeur héritant du constructeur
        définie dans la classe AbstractTest*/
        public TestChampsPublic(string name) : base(name) {}

        /*Implémentation de la méthode test*/
        override public bool test(){
            ChampsPublic cp = new ChampsPublic();
            cp.champspublic = 42;
            if (cp.champspublic == 42){
                return true;
            } else {
                return false;
            }
        }
    }
}



